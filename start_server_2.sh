#!/bin/sh
screen -S q2 -X select . > /dev/null 2>&1
if [ $? -eq 1 ]; then
	screen -A -m -d -S q2 \
		./cnq3-server-x64 \
		+set dedicated 2 \
		+set fs_game cpma \
		+set net_port 27961 \
		+set ttycon 1 \
		+set developer 0 \
		+exec server.cfg \
		+exec server.local.cfg.2 \
		+map cpm3a
fi

