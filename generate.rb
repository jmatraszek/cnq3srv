maps = %w(
)

template = <<TEMP
if [ ! -f XXXMAP.pk3 ]; then
    echo "Downloading XXXMAP map..."
    wget -N https://ws.q3df.org/maps/downloads/XXXMAP.pk3
    echo "XXXMAP map downloaded."
else
    echo "XXXMAP map already exists."
fi

TEMP
maps.each do |map|
  s = template.gsub("XXXMAP", map)
  puts s
end
