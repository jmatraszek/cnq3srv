#!/bin/bash

set -e

source map_lists.sh

DIR=${1:-baseq3}
cd $DIR

for i in "${q3df_maps[@]}"; do
    if [ ! -f $i.pk3 ]; then
        echo "Downloading $i.pk3..."
        wget -N https://ws.q3df.org/maps/downloads/$i.pk3
        echo "$i.pk3 downloaded."
    else
        echo "$i.pk3 already exist."
    fi
done
for i in "${fjo_maps[@]}"; do
    if [ ! -f $i.pk3 ]; then
        echo "Downloading $i..."
        wget -N https://fjoggs.com/maps/quake3/zips/$i.zip
        unzip $i.zip $i.pk3
        rm $i.zip
        echo "$i downloaded."
    else
        echo "$i already exist."
    fi
done
for i in "${discord_maps[@]}"; do
    if [ ! -f $i.pk3 ]; then
        echo "Discord map $i does not exist. Please download it from #leveldesign from CPMA Discord..."
    else
        echo "$i already exist."
    fi
done

if [ "$(hostname)" == 'ramen' ]; then
    # we are on cpma.ovh, maps uploaded manually. on maszyna download from cpma.ovh
    exit
fi

for i in "${discord_maps[@]}"; do
    if [ ! -f $i.pk3 ]; then
        echo "Downloading $i.pk3..."
        wget -N https://cpma.ovh/baseq3/$i.pk3
        echo "$i.pk3 downloaded."
    else
        echo "$i.pk3 already exist."
    fi
done
