#!/bin/bash

set -e

source map_lists.sh

DIR=${1:-baseq3}
cd $DIR

cpm_map="map_cpm*"
paks="pak*"

containsElement () { for e in "${@:2}"; do [[ "$e" = "$1" ]] && return 0; done; return 1; }

for file in *.pk3; do
    if [[ $file == $paks ]]; then
        continue
    fi
    if [[ $file == $cpm_map ]]; then
        continue
    fi
    if containsElement $(basename $file .pk3) "${all_maps[@]}"; then
        continue
    fi
    echo $file
    rm -i $file
done


