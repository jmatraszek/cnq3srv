#!/bin/sh
screen -S q3 -X select . > /dev/null 2>&1
if [ $? -eq 1 ]; then
	screen -A -m -d -S q3 \
		./cnq3-server-x64 \
		+set dedicated 1 \
		+set fs_game cpma \
		+set net_port 27962 \
		+set ttycon 1 \
		+set developer 0 \
		+exec server.cfg \
		+exec server.local.cfg.3 \
		+map darkztn
fi

