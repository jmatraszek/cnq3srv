#!/bin/sh

# Fail if any command fails
set -e

if [ ! -f cpma/z-cpma-pak152.pk3 ]; then
    echo "Downloading CPMA..."
    wget -N https://cdn.playmorepromode.com/files/cpma/cpma-1.52-nomaps.zip
    unzip cpma-1.52-nomaps.zip
    rm -rf cpma/stats cpma/*.txt cpma/*.ico cpma/hud
    rm cpma-1.52-nomaps.zip
    echo "CPMA downloaded."
else
    echo "CPMA already exists."
fi

if [ ! -f baseq3/pak1.pk3 ]; then
    echo "Downloading Quake 3 patch data..."
    wget -N https://www.ioquake3.org/data/quake3-latest-pk3s.zip --referer https://ioquake3.org/extras/patch-data/
    unzip -n quake3-latest-pk3s.zip
    mv quake3-latest-pk3s/baseq3/*.pk3 baseq3/
    mv quake3-latest-pk3s/missionpack/*.pk3 missionpack/
    rm -rf quake3-latest-pk3s
    rm quake3-latest-pk3s.zip
    echo "Quake 3 patch data downloaded."
else
    echo "Quake 3 patch data already exists."
fi

if [ ! -f baseq3/map_cpm3a.pk3 ]; then
    echo "Downloading CPMA mappack..."
    wget -N https://playmorepromode.org/files/cpma-mappack-full.zip
    mv cpma-mappack-full.zip baseq3/
    cd baseq3/
    unzip cpma-mappack-full.zip
    rm cpma-mappack-full.zip
    cd ..
    echo "CPMA mappack downloaded."
else
    echo "CPMA mappack already exists."
fi

if [ ! -f cnq3-server-x64 ]; then
    echo "Downloading CNQ3 server binary..."
    wget -N https://playmorepromode.org/files/latest/cnq3-1.51.zip
    unzip cnq3-1.51.zip
    chmod +x cnq3-server-x64
    rm cnq3-1.51.zip cnq3-server-*.exe cnq3-x* readme.txt changelog.txt
    echo "CNQ3 server binary downloaded."
else
    echo "CNQ3 server binary already exists."
fi

if [ ! -f baseq3/pak0.pk3 ]; then
    echo "baseq3/pak0.pk3 missing... please copy."
else
    echo "baseq3/pak0.pk3 exists."
fi

if [ ! -f baseq3/q3key ]; then
    echo "baseq3/q3key missing... please copy."
else
    echo "baseq3/q3key exists."
fi

echo "Installation successfull. Now run download_maps.sh to download some additional maps."
