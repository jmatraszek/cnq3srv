#!/bin/sh

# Fail if any command fails
set -e

rsync -va configs/cfg-maps/ cpma/cfg-maps --delete
rsync -va configs/classes/ cpma/classes --delete
rsync -va configs/locs/ cpma/locs --delete
rsync -va configs/modes/ cpma/modes --delete
rsync -va configs/viewcam/ cpma/viewcam --delete

echo "Configs updated successfully. Please restart server now."
